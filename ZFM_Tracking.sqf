/*
Zambino Fair Mission
Copyright (C) 2014,2015 Jordan Ashley Craw

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// This is appended to by missions
ZFM_DEFINES_TRACKING_MISSION_TIMEOUT_FUNCTIONS =[

];

ZFM_DEFINES_TRACKING_MISSION_TYPES =[];
ZFM_TRACKING_MISSION_STACK =[];

/*
*	ZFM_Tracking_Missions_ByType
*
*	Gets the number of missions in the stack currently by the mission type listed
*/
ZFM_Tracking_Missions_ByType ={
	private["_missionType","_numFound"];
	_missionType = _this select 0;

	_numFound = 0;

	{
		if((_x select 0) == _missionType) then
		{
			_numFound = _numFound +1;
		};
	} forEach ZFM_TRACKING_MISSION_STACK;

	_numFound
};



/*
*	ZFM_Tracking_Missions_CanAddNewMission
*
*	Tells us if we can add a new mission to the stack or not.
*/
ZFM_Tracking_Missions_CanAddNewMission ={
	((count ZFM_TRACKING_MISSION_STACK) < ZFM_CONFIGURE_MISSIONS_ALLTYPES_MAX_CONCURRENT_MISSIONS)
};


/*
	Mission Array

	0 = Type of mission
	1 = Datetime started
	2 = Datetime to expire
	3 = Mission parameters as an array
	4 = Units as an array
	5 = Objects as an array
	6 = Markers as an array
	7 = To be added
*/

/*
*	ZFM_Tracking_Missions_GetUnitsKilled
*
*	Checks how many units are killed and how many are there in total.
*/
ZFM_Tracking_Missions_GetUnitsKilled ={
	private["_missionID","_missionUnits","_unitsKilledCount"];
	_missionID = _this select 0;

	// Handle the mission ID checking.
	_missionExists = [_missionID] call ZFM_Tracking_Missions_MissionExists;
	if(not _missionExists) exitWith{[0,0]};

	// Get the units array
	_missionUnits = ZFM_TRACKING_MISSION_STACK select _missionID select 4;

	// Start counting.
	_unitsTotalCount = count _missionUnits;
	_unitsKilledCount = 0;

	// Loop the units
	{
		if(!(alive _x)) then
		{
			_unitsKilledCount = _unitsKilledCount +1;
		};
	} forEach _missionUnits;

	// 0 = killed, 1 = total
	[_unitsKilledCount,_unitsTotalCount]
};

/*
*	ZFM_Tracking_Missions_AllUnitsKilled
*
*	Checks if all units in a mission are killed.
*/
ZFM_Tracking_Missions_AllUnitsKilled = {	
	private["_missionID","_unitsKilled"];
	_missionID = _this select 0;

	// Get the units killed.
	_unitsKilled = [_missionID] call ZFM_Tracking_Missions_GetUnitsKilled;

	// Return whether the total is equal to the current killed
	((_unitsKilled select 0) == (_unitsKilled select 1))

};	


/*
*	ZFM_Tracking_Missions_HasExpired
*
*	Checks if a mission has expired.
*/
ZFM_Tracking_Missions_HasExpired = {
	private["_missionID"];
	_missionID = _this select 0;

	// Does the mission exist?
	_missionExists = [_missionID] call ZFM_Tracking_Missions_MissionExists;

	// Exit right away, technically if it's not there, it has expired. It has ceased to be.
	if(not _missionExists) exitWith{true};

	// Okay, so it exists, so has it expired, then?
	// Get the current time.
	_currentTime = diag_tickTime;

	if(_currentTime > (ZFM_TRACKING_MISSION_STACK select _missionID select 2)) then
	{
		if(true) exitWith{true};
	}
	else
	{
		if(true) exitWith{false};
	};

};

/*
*	ZFM_Tracking_Missions_MissionExists
*
*	Checks if a given mission exists.
*/
ZFM_Tracking_Missions_MissionExists ={
	(format["%1",(ZFM_TRACKING_MISSION_STACK select (_this select 0))] != "")
};

/*
*	ZFM_Tracking_Missions_PrepareNewMission
*
*	Prepares a new mission array to be added.
*/
ZFM_Tracking_Missions_AddNewMission ={
	private["_missionType","_missionExpireTimeMins","_missionParameters","_missionUnits","_missionObjects","_missionMarkers","_missionArray"];
	_missionType = _this select 0;
	_missionParameters = _this select 1;
	_missionUnits = _this select 2;
	_missionObjects = _this select 3;
	_missionMarkers = _this select 4;

	// Get the start time as tickTime (ms)
	_startTime = diag_tickTime;

	// Get the time this was supposed to start and add the expire time to it. 
	//_expireTime = _startTime + (ZFM_CONFIGURE_MISSIONS_ALLTYPES_EXPIRE_TIME*60000);
	_expireTime = _startTime + 60;

	// Generate the mission array with all the vitamins required for a growing mission..
	_missionArray = [
		_missionType,
		"CREATED",
		_startTime,
		_expireTime,
		_missionParameters,
		_missionUnits,
		_missionObjects,
		_missionMarkers
	];

	// Insert a new mission into the mission array.
	_inserted = _missionArray call ZFM_Tracking_Missions_InsertNewMission;

	// Did it work?
	_inserted
};


ZFM_TRACKING_KERNEL_CACHE_NUM_MISSIONS = 0;


ZFM_Tracking_Missions_TimeOut = {
	private["_continue"];
	_continue = true;

	while {_continue} do
	{
		waitUntil {count ZFM_TRACKING_MISSION_STACK >0};

		for [{_x=1},{_x<=count ZFM_TRACKING_MISSION_STACK-1},{_x=_x+1}] do
		{
			// Select the current row.
			_thisRow = ZFM_TRACKING_MISSION_STACK select _x;

			diag_log(format["%1",_thisRow]);

			if(diag_tickTime > _x select 2) then
			{
				format["Mission ID %1 has expired."] call ZFM_Common_Debug;

				// Need to create a timeout hook to handle this.
				[] spawn (call compile format["ZFM_Mission_Type_%1_End",_thisRow select 0]);
			};
		};

	};
};

ZFM_Tracking_Missions_Kernel ={
	private["_continue","_cachedNumberMissions"];
	_continue = true;

	format["Kernel initialised."] call ZFM_Common_Debug;

	while{_continue } do
	{
		_numMissions = count ZFM_TRACKING_MISSION_STACK;

		if(_numMissions == ZFM_TRACKING_KERNEL_CACHE_NUM_MISSIONS) then
		{
			// Debug log new mission waiting..
			format["Kernel waiting for a new mission to be added. %1 missions in stack.",_numMissions] call ZFM_Common_Debug;
			sleep 20; // Wait for the stack to update.
		}
		else
		{
			// Debug log new item.
			format["Kernel discovered new item in stack. Initialisation of mission starting.."] call ZFM_Common_Debug;			
			ZFM_TRACKING_KERNEL_CACHE_NUM_MISSIONS = _numMissions;

			// Run the mission init function

			sleep 20; // Wait for stack to update.
		};
	};

	
	
};

/*
*	ZFM_Tracking_Missions_InsertNewMission
*
*	Adds a new mission to the mission stack.
*/
ZFM_Tracking_Missions_InsertNewMission = {
	private["_missionArray","_canAddNewMission"];

	// Create the mission array
	_missionArray = _this;

	// Can we add a new mission?
	_canAddNewMission = call ZFM_Tracking_Missions_CanAddNewMission;

	if(_canAddNewMission) then
	{
		ZFM_TRACKING_MISSION_STACK set[(count ZFM_TRACKING_MISSION_STACK),_missionArray];
		if(true) exitWith{true};
	}
	else
	{
		if(true) exitWith{false};
	};
};