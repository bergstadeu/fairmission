/*
Zambino Fair Mission
Copyright (C) 2014,2015 Jordan Ashley Craw

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// Enable debug
ZFM_ENABLE_DEBUG_MESSAGES = true;

// The mod type you're using
ZFM_CONFIGURE_MOD_TYPE = "OverpochA2"; // "OverpochA2", "EpochA2", "EpochA3"

// Min missions to run at one time
ZFM_CONFIGURE_MISSIONS_ALLTYPES_MIN_CONCURRENT_MISSIONS = 1;

// Max missions to run at one time.
ZFM_CONFIGURE_MISSIONS_ALLTYPES_MAX_CONCURRENT_MISSIONS = 5;

// Expire time in minutes
ZFM_CONFIGURE_MISSIONS_ALLTYPES_EXPIRE_TIME = 60;